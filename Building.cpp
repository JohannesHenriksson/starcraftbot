#include "Building.h"


Building::Building(Unit* unit)
	:Agent(unit)
{
	agentType = Type_Building;
}


Building::~Building()
{
}

bool Building::Update()
{
	return Agent::Update();
}