#include "Factory.h"



Factory::Factory(Unit* unit)
:Building(unit)
{
	agentType = Type_Factory;
	hasAddon = false;
	unit->setRallyPoint(Global::baseChokePoint);
}


Factory::~Factory()
{
}


bool Factory::Update()
{
	if (!unit->isBeingConstructed())
	{
		if (hasAddon)
		{
			//Bygga Siege Tanks
			if (Global::nrOfSiegeTanks < Global::plannedSiegeTanks && !unit->isTraining() && !unit->isConstructing() && gotResources(UnitTypes::Terran_Siege_Tank_Tank_Mode))
			{
				//Broodwar->printf("Dags att bygga siege tank");
				unit->train(UnitTypes::Terran_Siege_Tank_Tank_Mode);
			}
		}
		else if (unit->isConstructing())
		{
			Broodwar->printf("Is Constructing, time left: %d", unit->getRemainingBuildTime());
			if (unit->getRemainingBuildTime() < 100)
			{
				hasAddon = true;
			}
		}
		else
		{
			if (gotResources(UnitTypes::Terran_Machine_Shop) && Broodwar->canMake(unit, UnitTypes::Terran_Machine_Shop))
			{
				if (unit->buildAddon(UnitTypes::Terran_Machine_Shop))
				{
				}
			}
		}
	}
	return Building::Update();
}

bool Factory::gotResources(UnitType unitType)
{
	if (unitType.gasPrice() > 0)
	{
		return(Broodwar->self()->minerals() - Global::plannedSpentMinerals >= unitType.mineralPrice() &&
			Broodwar->self()->gas() - Global::plannedSpentGas >= unitType.gasPrice());
	}
	else
		return(Broodwar->self()->minerals() - Global::plannedSpentMinerals >= unitType.mineralPrice());
}
