
#include "Global.h"

//Random Start Stuff
int Global::right;
int Global::left;
int Global::up;
int Global::down;
Position Global::startTCCPos;
Position Global::baseChokePoint;
Position Global::enemyBase;

//Minerals and Gas
int Global::plannedSpentMinerals;
int Global::plannedSpentGas = 0;

//Number of Units
int Global::nrOfWorkers = 0;
int Global::nrOfMarines = 0;
int Global::nrOfMedics = 0;
int Global::nrOfGasMiners = 0;
int Global::nrOfSiegeTanks = 0;

//Number of Buildings
int Global::nrOfCommandCenters = 1;
int Global::nrOfBarracks = 0;
int Global::nrOfSupplyDepots = 0;
int Global::nrOfRefineries = 0;
int Global::nrOfFactories = 0;
int Global::nrOfAcademies = 0;

//Planned Numbers
int Global::plannedGasMiners = 0;
int Global::plannedMarines = 10;
int Global::plannedSiegeTanks = 3;


std::vector<UnitType> Global::nextBuilding;