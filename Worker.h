#pragma once
#include "UnitAgent.h"
//#include "Source\ExampleAIModule.h"
#include "Global.h"
class Worker : 
	public UnitAgent
{
public:
	enum WorkMode
	{
		Mode_Mine,
		Mode_Scout,
		Mode_Build,
		Mode_Mine_Gas
	};

	Worker(){};
	Worker(Unit* unit);
	~Worker();
	bool Update();
	WorkMode mode;
	int WorkPhase;
	UnitType isBuilding;
	TilePosition buildPos;

	void SetScoutMode();
	bool CanBuildHere(TilePosition position, UnitType buildingType);
	bool IsTileReserved(TilePosition tilePos);
	TilePosition FindFreeBuildSlotSpiral(UnitType buildingType, Position spiralStartPos);
	void ReserveTilesForBuilding(UnitType BuildingType, TilePosition buildingPosition);

	static void RemoveTileFromReservedList(TilePosition tilePos);
	static std::vector<TilePosition> reservedTiles;
	Unit* closestRefinery;
};
