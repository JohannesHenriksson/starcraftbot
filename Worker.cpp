#include "Worker.h"

std::vector<TilePosition> Worker::reservedTiles;
//void Worker::RemoveTileFromReservedList(TilePosition tilePos);

Worker::Worker(Unit* unit)
	:UnitAgent(unit)
{
	agentType = Type_Worker;
	mode = Mode_Mine;
	WorkPhase = 0;
}


Worker::~Worker()
{
}

bool Worker::Update()
{
	switch (mode)
	{
	case Mode_Mine:
		if (Global::nextBuilding.size() != 0)
		{
			if (Global::nextBuilding[0].mineralPrice() < Broodwar->self()->minerals() - Global::plannedSpentMinerals)
			{
				Global::plannedSpentMinerals += Global::nextBuilding[0].mineralPrice();
				isBuilding = Global::nextBuilding[0];
				Global::nextBuilding.erase(Global::nextBuilding.begin());
				mode = Mode_Build;
				WorkPhase = 0;
				break;
			}
		}
		if (Global::nrOfGasMiners < Global::plannedGasMiners)
		{
			Global::nrOfGasMiners++;
			mode = Mode_Mine_Gas;
			unit->rightClick(Global::startTCCPos);
			break;
		}
		if (unit->isIdle())
		{
			Unit* closestMineral = NULL;
			for (std::set<Unit*>::iterator m = Broodwar->getMinerals().begin(); m != Broodwar->getMinerals().end(); m++)
			{
				if (closestMineral == NULL || unit->getDistance(*m) < unit->getDistance(closestMineral))
				{
					closestMineral = *m;
				}
			}
			if (closestMineral != NULL)
				unit->rightClick(closestMineral);
		}
		break;
	case Mode_Scout:
		if (unit->isIdle())
		{
			switch (WorkPhase)
			{
			case 0:
				unit->rightClick(Position(Global::startTCCPos.x(), Global::up + 30));
				break;
			case 1:
				unit->rightClick(Position(Global::right - 30, Global::startTCCPos.y()));
				break;
			case 2:
				unit->rightClick(Position(Global::startTCCPos.x(), Global::down - 30));
				break;
			case 3:
				unit->rightClick(Position(Global::left + 30, Global::startTCCPos.y()));
				break;
			default:
				Broodwar->printf("Default = %d", WorkPhase);
				mode = Mode_Mine;
				WorkPhase = -1;
				unit->rightClick(Position(Global::startTCCPos.x(), Global::up + 30));
				break;
			}
			WorkPhase++;
		}
		break;
	case Mode_Build:
		switch (WorkPhase)
		{
		case 0:
			buildPos = FindFreeBuildSlotSpiral(isBuilding, Global::startTCCPos);
			if (buildPos != TilePosition(-1, -1))
				if (unit->build(buildPos, isBuilding))
				{
					WorkPhase++;
					if (isBuilding == BWAPI::UnitTypes::Terran_Command_Center ||
						isBuilding == BWAPI::UnitTypes::Terran_Factory ||
						isBuilding == BWAPI::UnitTypes::Terran_Starport ||
						isBuilding == BWAPI::UnitTypes::Terran_Science_Facility)
					{
						ReserveTilesForBuilding(isBuilding, buildPos);
					}
				}
			else
			{
				Global::plannedSpentMinerals -= isBuilding.mineralPrice();
				Global::nextBuilding.insert(Global::nextBuilding.begin(), isBuilding);
				isBuilding = UnitTypes::None;
				mode = Mode_Mine;
				WorkPhase = 0;
			}
			break;
		case 1:
			if (unit->isConstructing() && unit->getBuildUnit() != NULL && unit->getBuildUnit()->getRemainingBuildTime() <= 400 && unit->getBuildUnit()->getRemainingBuildTime() > 50)
			{
				WorkPhase++;
			}
			else if (!unit->isConstructing())
			{
				Global::plannedSpentMinerals -= isBuilding.mineralPrice();
				Global::nextBuilding.insert(Global::nextBuilding.begin(), isBuilding);
				isBuilding = UnitTypes::None;
				mode = Mode_Mine;
				WorkPhase = 0;
			}
			break;
		case 2:
			if (unit->isConstructing())
			{
			}
			else
			{
				if (isBuilding == UnitTypes::Terran_Refinery)
				{
					Global::plannedSpentMinerals -= UnitTypes::Terran_Refinery.mineralPrice();
					mode = Mode_Mine_Gas;
					WorkPhase = 0;
					Global::plannedGasMiners += 3;
					Global::nrOfGasMiners++;
					ExampleAIModule::onRefineryCreate();
				}
				else
				{

					mode = Mode_Mine;
					WorkPhase = 0;
					unit->rightClick(Global::startTCCPos);
				}
				isBuilding = UnitTypes::None;
			}

			break;
		default:
			break;
		}
		break;
		/*		case 1:
			if (isBuilding == UnitTypes::Terran_Refinery)
			WorkPhase++;
			if (!unit->isConstructing())
			{
			Global::plannedSpentMinerals -= isBuilding.mineralPrice();
			Global::nextBuilding.insert(Global::nextBuilding.begin(), isBuilding);
			isBuilding = UnitTypes::None;
			mode = Mode_Mine;
			WorkPhase = 0;
			}
			break;*/

	case Mode_Mine_Gas:
		if (Global::nrOfGasMiners > Global::plannedGasMiners)
		{
			mode = Mode_Mine;
			WorkPhase = 0;
			Global::nrOfGasMiners--;
			unit->rightClick(Global::startTCCPos);
			break;
		}
		if (unit->isIdle())
		{
			if (closestRefinery != NULL)
			{
				unit->rightClick(closestRefinery);
			}
		}
		break;


	default:
		Broodwar->printf("Missing Worker Mode");
		break;
	}
	return Agent::Update();
}

void Worker::SetScoutMode()
{
	mode = Mode_Scout;
	//To get the worker idle
	unit->stop();
}



bool Worker::CanBuildHere(TilePosition position, UnitType buildingType)
{
	int width = buildingType.tileWidth();
	int height = buildingType.tileHeight();

	//plats f�r addons
	if (buildingType == BWAPI::UnitTypes::Terran_Command_Center ||
		buildingType == BWAPI::UnitTypes::Terran_Factory ||
		buildingType == BWAPI::UnitTypes::Terran_Starport ||
		buildingType == BWAPI::UnitTypes::Terran_Science_Facility)
	{
		width += 2;
	}
	int x = position.x();
	int y = position.y();
	for (int i = x; i < x + width; i++)
	{
		for (int j = y; j < y + height; j++)
		{
			if (i > Broodwar->mapWidth() || j > Broodwar->mapHeight() || j < 0 || i < 0 || !Broodwar->canBuildHere(NULL, TilePosition(i, j), buildingType, true) || !(Broodwar->getUnitsOnTile(i, j).size() == 0) || IsTileReserved(TilePosition(i, j)))
				return false;
		}
	}
	return true;
}

TilePosition Worker::FindFreeBuildSlotSpiral(UnitType buildingType, Position spiralStartPos)
{
	/*
	=====|=====|====|====|====|====|====|
	|	   |    |    |    |    |    |
	=====|=====|====|====|====|====|====|
	|	   |  6 |  5 | 4  |    |    |
	=====|=====|====|====|====|====|====|
	|	   |  7 |  C | 3  |    |    |
	=====|=====|====|====|====|====|====|
	|	   |  8 |  1 | 2  |    |    |
	=====|=====|====|====|====|====|====|
	|	   |  9 | 10 | 11 | 12 |    |
	=====|=====|====|====|====|====|====|
	|	   |    |    |    |    |    |*/

	if (buildingType == UnitTypes::Terran_Refinery)
	{
		Unit* closestGeyser = NULL;
		for (std::set<Unit*>::iterator m = Broodwar->getGeysers().begin(); m != Broodwar->getGeysers().end(); m++)
		{
			if (closestGeyser == NULL || unit->getDistance(*m) < unit->getDistance(closestGeyser))
			{
				closestGeyser = *m;
			}
		}
		return closestGeyser->getTilePosition();
		mode = Mode_Mine_Gas;
		Global::nrOfGasMiners++;
		ExampleAIModule::onRefineryCreate();
	}


	int length = 1;
	int x = spiralStartPos.x() / TILE_SIZE;
	int y = spiralStartPos.y() / TILE_SIZE;
	int dx = 0;
	int dy = 1;
	int currentLength = 0;
	bool changeLength = false;

	while (length < Broodwar->mapWidth())
	{
		if (CanBuildHere(TilePosition(x, y), buildingType))
			return TilePosition(x, y);

		x += dx;
		y += dy;
		currentLength++;

		if (currentLength == length)
		{
			currentLength = 0;

			if (changeLength)
				length++;

			changeLength = !changeLength;

			//Kontrollera v�ndningar i spiralen
			if (dx == 0)
			{
				dx = dy;
				dy = 0;
			}
			else
			{
				dy = -dx;
				dx = 0;
			}
		}


	}
	Broodwar->printf("Didn't find build spot");
	return TilePosition(-1, -1);
}

bool Worker::IsTileReserved(TilePosition tilePos)
{
	for (unsigned int c = 0; c < reservedTiles.size(); c++)
	{
		if (reservedTiles[c].x() == tilePos.x() && reservedTiles[c].y() == tilePos.y())
			return true;
	}
	return false;
}

void Worker::RemoveTileFromReservedList(TilePosition tilePos)
{
	std::vector<TilePosition>::iterator index = std::find(Worker::reservedTiles.begin(), Worker::reservedTiles.end(), tilePos);
	if (index != Worker::reservedTiles.end())
		Worker::reservedTiles.erase(index);
}

void Worker::ReserveTilesForBuilding(UnitType buildingType, TilePosition buildingPosition)
{
	int width = buildingType.tileWidth();
	for (int c = 0; c < 2; c++)
	{
		for (int k = 0; k < 2; k++)
		{
			Worker::reservedTiles.push_back(TilePosition((buildingPosition.x() + width + c), (buildingPosition.y() + k)));
		}
	}
}
