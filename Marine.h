#pragma once
#include "UnitAgent.h"
class Marine :
	public UnitAgent
{
public:
	Marine();
	Marine(Unit* unit);
	~Marine();
	bool Update();
};

