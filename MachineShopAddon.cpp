#include "MachineShopAddon.h"




MachineShopAddon::MachineShopAddon(Unit* unit)
:Building(unit)
{
	agentType = Type_MachineShopAddon;
}


MachineShopAddon::~MachineShopAddon()
{
}

bool MachineShopAddon::Update()
{

	if (!unit->isConstructing())
	{
		if (!unit->isUpgrading() && !Broodwar->self()->hasResearched(TechTypes::Tank_Siege_Mode) &&
			Broodwar->self()->minerals() - Global::plannedSpentMinerals >= TechTypes::Tank_Siege_Mode.mineralPrice() &&
			Broodwar->self()->gas() - Global::plannedSpentGas >= TechTypes::Tank_Siege_Mode.mineralPrice())
		{
			unit->research(TechTypes::Tank_Siege_Mode);
		}
	}

	return Building::Update();
}
