#include "../Agent.h"
#include "ExampleAIModule.h" 
using namespace BWAPI;

bool analyzed;
bool analysis_just_finished;
BWTA::Region* home;
BWTA::Region* enemy_base;
bool drawSlot = false;
TilePosition drawTile;
int phase;
int barracksCount = 0;


/*
int Global::right;
int Global::left;
int Global::up;
int Global::down;
Position Global::startTCCPos;
int Global::plannedSpentMinerals;
std::vector<UnitType> Global::nextBuilding;*/

//int ExampleAIModule::nrOfMarines;

bool hasSentScout = false;

std::vector<Agent*> AgentList;
std::vector<Worker> buildingWorkers;

//This is the startup method. It is called once
//when a new game has been started with the bot.
void ExampleAIModule::onStart()
{
	Broodwar->sendText("Hello world!");
	//Enable flags
	Broodwar->enableFlag(Flag::UserInput);
	//Uncomment to enable complete map information
	Broodwar->enableFlag(Flag::CompleteMapInformation);

	//Start analyzing map data
	BWTA::readMap();
	analyzed = false;
	analysis_just_finished = false;
	//CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)AnalyzeThread, NULL, 0, NULL); //Threaded version
	AnalyzeThread();

	phase = 0;
	depotInQueue = true;
	academyHasBeenBuilt = false;
	//Planned spent minerals kommer tas bort p� onUnitCreate
	Global::plannedSpentMinerals = UnitTypes::Terran_Command_Center.mineralPrice();
	Global::nextBuilding.push_back(UnitTypes::Terran_Supply_Depot);
	Global::nextBuilding.push_back(UnitTypes::Terran_Barracks);
	Global::nextBuilding.push_back(UnitTypes::Terran_Refinery);
	Global::baseChokePoint = findGuardPoint();

	buildingWorkers = std::vector<Worker>();
	//Initialize box around home town
	for (std::set<Unit*>::const_iterator i = Broodwar->self()->getUnits().begin(); i != Broodwar->self()->getUnits().end(); i++)
	{
		if ((*i)->getType() == UnitTypes::Terran_Command_Center)
		{
			Global::startTCCPos = (*i)->getPosition();
			Position end = Global::startTCCPos;
			while (end.hasPath(Global::startTCCPos))
				end.x()++;
			Global::right = end.x();
			end = Global::startTCCPos;
			while (end.hasPath(Global::startTCCPos))
				end.x()--;
			Global::left = end.x();
			end = Global::startTCCPos;
			while (end.hasPath(Global::startTCCPos))
				end.y()--;
			Global::up = end.y();
			end = Global::startTCCPos;
			while (end.hasPath(Global::startTCCPos))
				end.y()++;
			Global::down = end.y();
		}
	}
	Global::enemyBase = enemy_base->getCenter();
}

//Called when a game is ended.
//No need to change this.
void ExampleAIModule::onEnd(bool isWinner)
{
	if (isWinner)
	{
		Broodwar->sendText("I won!");
	}
	else
	{
		Broodwar->sendText("GG WP");
}
}

//Finds a guard point around the home base.
//A guard point is the center of a chokepoint surrounding
//the region containing the home base.
Position ExampleAIModule::findGuardPoint()
{
	//Get the chokepoints linked to our home region
	std::set<BWTA::Chokepoint*> chokepoints = home->getChokepoints();
	double min_length = 10000;
	BWTA::Chokepoint* choke = NULL;

	//Iterate through all chokepoints and look for the one with the smallest gap (least width)
	for(std::set<BWTA::Chokepoint*>::iterator c = chokepoints.begin(); c != chokepoints.end(); c++)
	{
		double length = (*c)->getWidth();
		if (length < min_length || choke==NULL)
		{
			min_length = length;
			choke = *c;
		}
	}

	return choke->getCenter();
}

//This is the method called each frame. This is where the bot's logic
//shall be called.
void ExampleAIModule::onFrame()
{
	//Send scout at start of game
	if (!hasSentScout)
		for (unsigned int c = 0; c < AgentList.size(); c++)
			if (AgentList[c]->getAgentType() == Agent::Type_Worker)
			{
				Broodwar->printf("Ska s�tt scout scv");
				static_cast<Worker*>(AgentList[c])->SetScoutMode();
				hasSentScout = true;
				break;
			}

	//Call every 20:th frame
	if (Broodwar->getFrameCount() % 20 == 0)
	{
		for (unsigned int i = 0; i < AgentList.size(); i++)
		{
			Agent::AgentType agentType = AgentList[i]->getAgentType();
			switch (agentType)
			{
			case::Agent::Type_Worker:
				Worker* worker;
				worker = static_cast<Worker*>(AgentList[i]);
				if (!worker->Update())
				{
					Broodwar->printf("REMOVE WORKER FROM AGENTLIST");
					AgentList.erase(AgentList.begin() + i);
					i--;
					delete(worker);
				}
/*
				if (worker->mode == Worker::Mode_Build && worker->isBuilding != UnitTypes::Terran_Refinery)
					buildingWorkers.push_back(*worker);*/
				if (!academyHasBeenBuilt && Broodwar->canMake(worker->getUnit(), UnitTypes::Terran_Academy))
				{
					Global::nextBuilding.push_back(UnitTypes::Terran_Academy);
					Global::nextBuilding.push_back(UnitTypes::Terran_Factory);
					academyHasBeenBuilt = true;
				}
				break;

			case::Agent::Type_Building:
				break;
			case::Agent::Type_CommandCenter:
				TCC* commandCenter;
				commandCenter = static_cast<TCC*>(AgentList[i]);
				if (!commandCenter->Update())
				{
					AgentList.erase(AgentList.begin() + i);
					i--;
					delete(commandCenter);
				}
				break;
			case::Agent::Type_Barrack:
				Barrack* barrack;
				barrack = static_cast<Barrack*>(AgentList[i]);
				if (!barrack->Update())
				{
					AgentList.erase(AgentList.begin() + i);
					i--;
					delete(barrack);
				}
				break;
			case::Agent::Type_Factory:
				Factory* factory;
				factory = static_cast<Factory*>(AgentList[i]);
				if (!factory->Update())
				{
					AgentList.erase(AgentList.begin() + i);
					i--;
					delete(factory);
				}
				break;
			case::Agent::Type_MachineShopAddon:
				MachineShopAddon* machineShop;
				machineShop = static_cast<MachineShopAddon*>(AgentList[i]);
				if (!machineShop->Update())
				{
					AgentList.erase(AgentList.begin() + i);
					i--;
					delete(machineShop);
				}
				break;
			case::Agent::Type_Unit:
				break;
			case::Agent::Type_MilitaryUnit:
				break;
			case::Agent::Type_Marine:

				Marine* marine;
				marine = static_cast<Marine*>(AgentList[i]);
				if (!marine->Update())
				{
					AgentList.erase(AgentList.begin() + i);
					i--;
					delete(marine);
				}
				break;
			case::Agent::Type_SiegeTank:
				SiegeTank* siegeTank;
				siegeTank = static_cast<SiegeTank*>(AgentList[i]);
				if (!siegeTank->Update())
				{
					AgentList.erase(AgentList.begin() + i);
					i--;
					delete(siegeTank);
				}
				break;
			case::Agent::Type_Academy:
				Academy* academy;
				academy = static_cast<Academy*>(AgentList[i]);
				if (!academy->Update())
				{
					AgentList.erase(AgentList.begin() + i);
					i--;
					delete(academy);
				}
				break;
			case::Agent::Type_Medic:
				Medic* medic;
				medic = static_cast<Medic*>(AgentList[i]);
				if (!medic->Update())
				{
					AgentList.erase(AgentList.begin() + i);
					i--;
					delete(medic);
				}
				break;
			case::Agent::Type_Refinery:
				Refinery* refinery;
				refinery = static_cast<Refinery*>(AgentList[i]);
				if (!refinery->Update())
				{
					AgentList.erase(AgentList.begin() + i);
					i--;
					delete(refinery);
				}
				break;
			case::Agent::Type_Undefined:
				Broodwar->printf("Undefined Agent Type");
				break;

			default:
				Broodwar->printf("Missing Agent Type");
				break;
			}
		}

		if ((Broodwar->self()->supplyTotal()/2) - (Broodwar->self()->supplyUsed()/2) <= 4 && !depotInQueue)
		{
			Global::nextBuilding.insert(Global::nextBuilding.begin(), 1, UnitTypes::Terran_Supply_Depot);
			depotInQueue = true;
		}
		if ((Broodwar->self()->supplyTotal() == Broodwar->self()->supplyUsed()) && !depotInQueue)
		{
			Global::nextBuilding.insert(Global::nextBuilding.begin(), 1, UnitTypes::Terran_Supply_Depot);
			depotInQueue = true;
		}
	}

	//Draw lines around regions, chokepoints etc.
	if (analyzed)
	{
		drawTerrainData();
	}
}

//Is called when text is written in the console window.
//Can be used to toggle stuff on and off.
void ExampleAIModule::onSendText(std::string text)
{
	if (text=="/show players")
	{
		showPlayers();
	}
	else if (text=="/show forces")
	{
		showForces();
	}
	else if (text == "/getinfo")
	{
		Broodwar->printf("Planned Spent Minerals: %d\nPlanned Spent Gas: %d\nNumber of buildings in queue: %d", Global::plannedSpentMinerals, Global::plannedSpentGas, Global::nextBuilding.size());
		if (Global::nextBuilding.size() > 0)
		{
			Broodwar->printf("Building Queue: ");
			for (unsigned int c = 0; c < Global::nextBuilding.size(); c++)
			{
				if (Global::nextBuilding[c] == UnitTypes::Terran_Academy)
					Broodwar->printf("Academy");
				if (Global::nextBuilding[c] == UnitTypes::Terran_Supply_Depot)
					Broodwar->printf("Supply Depot");
				if (Global::nextBuilding[c] == UnitTypes::Terran_Barracks)
					Broodwar->printf("Barracks");
				if (Global::nextBuilding[c] == UnitTypes::Terran_Refinery)
					Broodwar->printf("Refinery");
				if (Global::nextBuilding[c] == UnitTypes::Terran_Factory)
					Broodwar->printf("Factory");
			}
		}
		if (depotInQueue)
			Broodwar->printf("Depot in queue - true");
	}
	else
	{
		Broodwar->printf("You typed '%s'!",text.c_str());
		Broodwar->sendText("%s",text.c_str());
	}
}

//Called when the opponent sends text messages.
//No need to change this.
void ExampleAIModule::onReceiveText(BWAPI::Player* player, std::string text)
{
	Broodwar->printf("%s said '%s'", player->getName().c_str(), text.c_str());
}

//Called when a player leaves the game.
//No need to change this.
void ExampleAIModule::onPlayerLeft(BWAPI::Player* player)
{
	Broodwar->sendText("%s left the game.",player->getName().c_str());
}

//Called when a nuclear launch is detected.
//No need to change this.
void ExampleAIModule::onNukeDetect(BWAPI::Position target)
{
	if (target!=Positions::Unknown)
	{
		Broodwar->printf("Nuclear Launch Detected at (%d,%d)",target.x(),target.y());
	}
	else
	{
		Broodwar->printf("Nuclear Launch Detected");
	}
}

//No need to change this.
void ExampleAIModule::onUnitDiscover(BWAPI::Unit* unit)
{
	//Broodwar->sendText("A %s [%x] has been discovered at (%d,%d)",unit->getType().getName().c_str(),unit,unit->getPosition().x(),unit->getPosition().y());
}

//No need to change this.
void ExampleAIModule::onUnitEvade(BWAPI::Unit* unit)
{
	//Broodwar->sendText("A %s [%x] was last accessible at (%d,%d)",unit->getType().getName().c_str(),unit,unit->getPosition().x(),unit->getPosition().y());
}

//No need to change this.
void ExampleAIModule::onUnitShow(BWAPI::Unit* unit)
{
	//Broodwar->sendText("A %s [%x] has been spotted at (%d,%d)",unit->getType().getName().c_str(),unit,unit->getPosition().x(),unit->getPosition().y());
}

//No need to change this.
void ExampleAIModule::onUnitHide(BWAPI::Unit* unit)
{
	//Broodwar->sendText("A %s [%x] was last seen at (%d,%d)",unit->getType().getName().c_str(),unit,unit->getPosition().x(),unit->getPosition().y());
}

//Called when a new unit has been created.
//Note: The event is called when the new unit is built, not when it
//has been finished.
void ExampleAIModule::onUnitCreate(BWAPI::Unit* unit)
{
	if (unit->getPlayer() == Broodwar->self())
	{
		Broodwar->sendText("A %s [%x] has been created at (%d,%d)",unit->getType().getName().c_str(),unit,unit->getPosition().x(),unit->getPosition().y());
		UnitType createdUnitType = unit->getType();

		if (createdUnitType == UnitTypes::Terran_Barracks)
		{
			Global::plannedSpentMinerals -= createdUnitType.mineralPrice();
			Global::nrOfBarracks++;
			Barrack* newBarrack = new Barrack(unit);
			AgentList.push_back(newBarrack);
/*
			for (unsigned int c = 0; c < buildingWorkers.size(); c++)
			{
				if (buildingWorkers[c].getUnit() == newBarrack->getUnit()->getBuildUnit() && buildingWorkers[c].WorkPhase == 1)
				{
					buildingWorkers[c].WorkPhase++;
					buildingWorkers.erase(buildingWorkers.begin() + c);
					c--;
				}
			}*/
			
		}
		else if (createdUnitType == UnitTypes::Terran_SCV)
		{
			Global::nrOfWorkers++;
			Worker* newSCV = new Worker(unit);
			AgentList.push_back(newSCV);
		}
		else if (createdUnitType == UnitTypes::Terran_Marine)
		{
			Global::nrOfMarines++;
			Marine* newMarine = new Marine(unit);
			AgentList.push_back(newMarine);
		}
		else if (createdUnitType == UnitTypes::Terran_Command_Center)
		{
			Global::plannedSpentMinerals -= createdUnitType.mineralPrice();
			Global::nrOfCommandCenters++;
			TCC* newCommandCenter = new TCC(unit);
			AgentList.push_back(newCommandCenter);
/*
			if (Global::nrOfCommandCenters > 1)
			{

				for (unsigned int c = 0; c < buildingWorkers.size(); c++)
				{
					if (buildingWorkers[c].getUnit() == newCommandCenter->getUnit()->getBuildUnit() && buildingWorkers[c].WorkPhase == 1)
					{
						buildingWorkers[c].WorkPhase++;
						buildingWorkers.erase(buildingWorkers.begin() + c);
						c--;
					}
				}
			}*/
		}
		else if (createdUnitType == UnitTypes::Terran_Academy)
		{
			Academy* newAcademy = new Academy(unit);
			AgentList.push_back(newAcademy);
			Global::nrOfAcademies++;
			Global::plannedSpentMinerals -= createdUnitType.mineralPrice();
/*
			for (unsigned int c = 0; c < buildingWorkers.size(); c++)
			{
				if (buildingWorkers[c].getUnit() == newAcademy->getUnit()->getBuildUnit() && buildingWorkers[c].WorkPhase == 1)
				{
					buildingWorkers[c].WorkPhase++;
					buildingWorkers.erase(buildingWorkers.begin() + c);
					c--;
				}
			}*/
		}
		else if (createdUnitType == UnitTypes::Terran_Refinery)
		{
			Broodwar->printf("Refinery Created");
			Refinery* newRefinery = new Refinery(unit);
			AgentList.push_back(newRefinery);
			Global::nrOfRefineries++;
			Global::plannedSpentMinerals -= createdUnitType.mineralPrice();
			Global::plannedGasMiners += 3;
/*
			for (unsigned int i = 0; i < AgentList.size(); i++)
			{
				if (AgentList[i]->getAgentType() == Agent::Type_Worker)
				{
					static_cast<Worker*>(AgentList[i])->closestRefinery = unit;
				}
			}*/
		}
		else if (createdUnitType == UnitTypes::Terran_Medic)
		{
			Medic* newMedic = new Medic(unit);
			AgentList.push_back(newMedic);
			Global::nrOfMedics++;
		}
		else if (createdUnitType == UnitTypes::Terran_Siege_Tank_Tank_Mode)
		{
			Global::nrOfSiegeTanks++;
			SiegeTank* siegetank = new SiegeTank(unit);
			AgentList.push_back(siegetank);

		}
		else if (createdUnitType == UnitTypes::Terran_Factory)
		{
			Global::plannedSpentMinerals -= createdUnitType.mineralPrice();
			Factory* factory = new Factory(unit);
			AgentList.push_back(factory);
			Global::nrOfFactories++;
/*
			for (unsigned int c = 0; c < buildingWorkers.size(); c++)
			{
				if (buildingWorkers[c].getUnit() == factory->getUnit()->getBuildUnit() && buildingWorkers[c].WorkPhase == 1)
				{
					buildingWorkers[c].WorkPhase++;
					buildingWorkers.erase(buildingWorkers.begin() + c);
					c--;
				}
			}*/
		}
		else if (createdUnitType == UnitTypes::Terran_Supply_Depot)
		{
			Global::plannedSpentMinerals -= createdUnitType.mineralPrice();
			Global::nrOfSupplyDepots++;
			Building* supplyDepot = new Building(unit);
			AgentList.push_back(supplyDepot);
/*
			for (unsigned int c = 0; c < buildingWorkers.size(); c++)
			{
				if (buildingWorkers[c].getUnit() == supplyDepot->getUnit()->getBuildUnit() && buildingWorkers[c].WorkPhase == 1)
				{
					buildingWorkers[c].WorkPhase++;
					buildingWorkers.erase(buildingWorkers.begin() + c);
					c--;
				}
			}*/
		}
		else if (createdUnitType == UnitTypes::Terran_Machine_Shop)
		{
			MachineShopAddon* machineShop = new MachineShopAddon(unit);
			AgentList.push_back(machineShop);
		}
	}
}

void ExampleAIModule::onRefineryCreate()
{
	Broodwar->printf("Refinery Created");
	for (std::set<Unit*>::const_iterator i = Broodwar->self()->getUnits().begin(); i != Broodwar->self()->getUnits().end(); i++)
	{
		if ((*i)->getType() == UnitTypes::Terran_Refinery)
		{
			Broodwar->printf("Refinery Created");
			Refinery* newRefinery = new Refinery(*i);
			AgentList.push_back(newRefinery);
			Global::nrOfRefineries++;
			for (unsigned int c = 0; c < AgentList.size(); c++)
			{
				if (AgentList[c]->getAgentType() == Agent::Type_Worker)
				{
					static_cast<Worker*>(AgentList[c])->closestRefinery = (*i);
				}
			}
		}
	}
}

//Called when a unit has been destroyed.
void ExampleAIModule::onUnitDestroy(BWAPI::Unit* unit)
{
	if (unit->getPlayer() == Broodwar->self())
	{
		UnitType destroyedUnitType = unit->getType();

		if (destroyedUnitType == UnitTypes::Terran_Barracks)
		{
			Global::nrOfBarracks--;
			Global::nextBuilding.insert(Global::nextBuilding.begin(), 1, destroyedUnitType);
		}
		else if (destroyedUnitType == UnitTypes::Terran_SCV)
		{
			Global::nrOfWorkers--;
		}
		else if (destroyedUnitType == UnitTypes::Terran_Marine)
		{
			Global::nrOfMarines--;
		}
		else if (destroyedUnitType == UnitTypes::Terran_Command_Center)
		{
			Global::nrOfCommandCenters--;
			Global::nextBuilding.insert(Global::nextBuilding.begin(), 1, destroyedUnitType);
		}
		else if (destroyedUnitType == UnitTypes::Terran_Academy)
		{
			Global::nextBuilding.insert(Global::nextBuilding.begin(), 1, destroyedUnitType);
		}
		else if (destroyedUnitType == UnitTypes::Terran_Refinery)
		{

			Global::plannedGasMiners -= 3;
			Global::nextBuilding.insert(Global::nextBuilding.begin(), 1, destroyedUnitType);
		}
		else if (destroyedUnitType == UnitTypes::Terran_Medic)
		{
			Global::nrOfMedics--;
		}
		else if (destroyedUnitType == UnitTypes::Terran_Siege_Tank_Tank_Mode)
		{
			Global::nrOfSiegeTanks--;
		}
		else if (destroyedUnitType == UnitTypes::Terran_Factory)
		{
			Global::nrOfFactories--;
			Global::nextBuilding.insert(Global::nextBuilding.begin(), 1, destroyedUnitType);
		}
		else if (destroyedUnitType == UnitTypes::Terran_Supply_Depot)
		{
			Global::nrOfSupplyDepots--;
		}
		else
		{
			Broodwar->printf("Unknown unit type has been destroyed: %s", destroyedUnitType.getName());
		}
		Broodwar->sendText("My unit %s [%x] has been destroyed at (%d,%d)", destroyedUnitType.getName().c_str(), unit, unit->getPosition().x(), unit->getPosition().y());
		if (destroyedUnitType == BWAPI::UnitTypes::Terran_Command_Center ||
			destroyedUnitType == BWAPI::UnitTypes::Terran_Factory ||
			destroyedUnitType == BWAPI::UnitTypes::Terran_Starport ||
			destroyedUnitType == BWAPI::UnitTypes::Terran_Science_Facility)
		{
			TilePosition buildingPos = unit->getTilePosition();
			int width = unit->getType().tileWidth();
			int height = unit->getType().tileHeight();
			for (int c = 0; c < 2; c++)
			{
				for (int k = 0; k < 2; k++)
				{
					Worker::RemoveTileFromReservedList(TilePosition((buildingPos.x() + width + c), (buildingPos.y() + k)));
				}
			}
		}
		
	}
	else
	{
		Broodwar->sendText("Enemy unit %s [%x] has been destroyed at (%d,%d)",unit->getType().getName().c_str(),unit,unit->getPosition().x(),unit->getPosition().y());
	}
}

//Only needed for Zerg units.
//No need to change this.
void ExampleAIModule::onUnitMorph(BWAPI::Unit* unit)
{
	//Broodwar->sendText("A %s [%x] has been morphed at (%d,%d)",unit->getType().getName().c_str(),unit,unit->getPosition().x(),unit->getPosition().y());
}

//No need to change this.
void ExampleAIModule::onUnitRenegade(BWAPI::Unit* unit)
{
	//Broodwar->sendText("A %s [%x] is now owned by %s",unit->getType().getName().c_str(),unit,unit->getPlayer()->getName().c_str());
}

//No need to change this.
void ExampleAIModule::onSaveGame(std::string gameName)
{
	Broodwar->printf("The game was saved to \"%s\".", gameName.c_str());
}

//Analyzes the map.
//No need to change this.
DWORD WINAPI AnalyzeThread()
{
	BWTA::analyze();

	//Self start location only available if the map has base locations
	if (BWTA::getStartLocation(BWAPI::Broodwar->self())!=NULL)
	{
		home = BWTA::getStartLocation(BWAPI::Broodwar->self())->getRegion();
	}
	//Enemy start location only available if Complete Map Information is enabled.
	if (BWTA::getStartLocation(BWAPI::Broodwar->enemy())!=NULL)
	{
		enemy_base = BWTA::getStartLocation(BWAPI::Broodwar->enemy())->getRegion();
	}
	analyzed = true;
	analysis_just_finished = true;
	return 0;
}

//Prints some stats about the units the player has.
//No need to change this.
void ExampleAIModule::drawStats()
{
	std::set<Unit*> myUnits = Broodwar->self()->getUnits();
	Broodwar->drawTextScreen(5,0,"I have %d units:",myUnits.size());
	std::map<UnitType, int> unitTypeCounts;
	for(std::set<Unit*>::iterator i=myUnits.begin();i!=myUnits.end();i++)
	{
		if (unitTypeCounts.find((*i)->getType())==unitTypeCounts.end())
		{
			unitTypeCounts.insert(std::make_pair((*i)->getType(),0));
		}
		unitTypeCounts.find((*i)->getType())->second++;
	}
	int line=1;
	for(std::map<UnitType,int>::iterator i=unitTypeCounts.begin();i!=unitTypeCounts.end();i++)
	{
		Broodwar->drawTextScreen(5,16*line,"- %d %ss",(*i).second, (*i).first.getName().c_str());
		line++;
	}
}

//Draws terrain data aroung regions and chokepoints.
//No need to change this.
void ExampleAIModule::drawTerrainData()
{
	//Iterate through all the base locations, and draw their outlines.
	for(std::set<BWTA::BaseLocation*>::const_iterator i=BWTA::getBaseLocations().begin();i!=BWTA::getBaseLocations().end();i++)
	{
		TilePosition p=(*i)->getTilePosition();
		Position c=(*i)->getPosition();
		//Draw outline of center location
		Broodwar->drawBox(CoordinateType::Map,p.x()*32,p.y()*32,p.x()*32+4*32,p.y()*32+3*32,Colors::Blue,false);
		//Draw a circle at each mineral patch
		for(std::set<BWAPI::Unit*>::const_iterator j=(*i)->getStaticMinerals().begin();j!=(*i)->getStaticMinerals().end();j++)
		{
			Position q=(*j)->getInitialPosition();
			Broodwar->drawCircle(CoordinateType::Map,q.x(),q.y(),30,Colors::Cyan,false);
		}
		//Draw the outlines of vespene geysers
		for(std::set<BWAPI::Unit*>::const_iterator j=(*i)->getGeysers().begin();j!=(*i)->getGeysers().end();j++)
		{
			TilePosition q=(*j)->getInitialTilePosition();
			Broodwar->drawBox(CoordinateType::Map,q.x()*32,q.y()*32,q.x()*32+4*32,q.y()*32+2*32,Colors::Orange,false);
		}
		//If this is an island expansion, draw a yellow circle around the base location
		if ((*i)->isIsland())
		{
			Broodwar->drawCircle(CoordinateType::Map,c.x(),c.y(),80,Colors::Yellow,false);
		}
	}
	//Iterate through all the regions and draw the polygon outline of it in green.
	for(std::set<BWTA::Region*>::const_iterator r=BWTA::getRegions().begin();r!=BWTA::getRegions().end();r++)
	{
		BWTA::Polygon p=(*r)->getPolygon();
		for(int j=0;j<(int)p.size();j++)
		{
			Position point1=p[j];
			Position point2=p[(j+1) % p.size()];
			Broodwar->drawLine(CoordinateType::Map,point1.x(),point1.y(),point2.x(),point2.y(),Colors::Green);
		}
	}
	//Visualize the chokepoints with red lines
	for(std::set<BWTA::Region*>::const_iterator r=BWTA::getRegions().begin();r!=BWTA::getRegions().end();r++)
	{
		for(std::set<BWTA::Chokepoint*>::const_iterator c=(*r)->getChokepoints().begin();c!=(*r)->getChokepoints().end();c++)
		{
			Position point1=(*c)->getSides().first;
			Position point2=(*c)->getSides().second;
			Broodwar->drawLine(CoordinateType::Map,point1.x(),point1.y(),point2.x(),point2.y(),Colors::Red);
		}
	}

	Broodwar->drawBox(CoordinateType::Map, Global::left, Global::up, Global::right, Global::down, Colors::Blue, false);
}

//Show player information.
//No need to change this.
void ExampleAIModule::showPlayers()
{
	std::set<Player*> players=Broodwar->getPlayers();
	for(std::set<Player*>::iterator i=players.begin();i!=players.end();i++)
	{
		Broodwar->printf("Player [%d]: %s is in force: %s",(*i)->getID(),(*i)->getName().c_str(), (*i)->getForce()->getName().c_str());
	}
}

//Show forces information.
//No need to change this.
void ExampleAIModule::showForces()
{
	std::set<Force*> forces=Broodwar->getForces();
	for(std::set<Force*>::iterator i=forces.begin();i!=forces.end();i++)
	{
		std::set<Player*> players=(*i)->getPlayers();
		Broodwar->printf("Force %s has the following players:",(*i)->getName().c_str());
		for(std::set<Player*>::iterator j=players.begin();j!=players.end();j++)
		{
			Broodwar->printf("  - Player [%d]: %s",(*j)->getID(),(*j)->getName().c_str());
		}
	}
}

//Called when a unit has been completed, i.e. finished built.
void ExampleAIModule::onUnitComplete(BWAPI::Unit *unit)
{
	if (unit->getPlayer() == Broodwar->self())
	{
		UnitType createdUnitType = unit->getType();

		if (createdUnitType == UnitTypes::Terran_Supply_Depot)
		{
			depotInQueue = false;
		}
	}
	//Broodwar->sendText("A %s [%x] has been completed at (%d,%d)",unit->getType().getName().c_str(),unit,unit->getPosition().x(),unit->getPosition().y());
}


void ExampleAIModule::buildSupplyDepot()
{
	TilePosition tilePos;

	
	for (std::set<Unit*>::const_iterator i = Broodwar->self()->getUnits().begin(); i != Broodwar->self()->getUnits().end(); i++)
	{
		//Check if unit is a worker.
		if ((*i)->getType().isWorker() && !(*i)->isConstructing())
		{
			tilePos = FindFreeBuildSlotInHomeRegion(UnitTypes::Terran_Supply_Depot);
			Broodwar->printf("Building worker type: %s", (*i)->getType().getName());
			if (!Broodwar->isExplored(tilePos))
			{
				Broodwar->printf("Not discovered");
				(*i)->rightClick(Position(tilePos.x()*TILE_SIZE, tilePos.y()*TILE_SIZE));
			}
			if (!(*i)->build(tilePos, UnitTypes::Terran_Supply_Depot))
			{
				drawSlot = true;
				drawTile = tilePos;
				Broodwar->printf("Failed to build");
			}
			break;
		}
	}

}

bool ExampleAIModule::Build(UnitType buildingType, TilePosition buildingPosition)
{
	for (std::set<Unit*>::const_iterator i = Broodwar->self()->getUnits().begin(); i != Broodwar->self()->getUnits().end(); i++)
	{
		//Check if unit is a worker.
		if ((*i)->getType().isWorker() && !(*i)->isConstructing())
		{
			return (*i)->build(buildingPosition, buildingType);
		}
	}
	return false;
}

bool ExampleAIModule::Build(UnitType buildingType)
{
	Position newPos = home->getCenter();
	TilePosition pos;
	for (std::set<Unit*>::const_iterator i = Broodwar->self()->getUnits().begin(); i != Broodwar->self()->getUnits().end(); i++)
	{
		//Check if unit is a worker and not currently constructing a building.
		if ((*i)->getType().isWorker() && !(*i)->isConstructing())
		{
			pos = FindFreeBuildSlotSpiral(buildingType, Global::startTCCPos);
			if (pos.x() == -1)
				Broodwar->printf("Ingen position hittades");
			if (Build(buildingType, pos))
			{
				Global::plannedSpentMinerals += buildingType.mineralPrice();
				return true;
			}
			else
			{
				if ((*i)->getPosition().hasPath(Position(pos.x()*TILE_SIZE, pos.y()*TILE_SIZE)))
				{
					(*i)->rightClick(Position(pos.x()*TILE_SIZE, pos.y()*TILE_SIZE));
					Broodwar->printf("Rightclick mot (%d, %d)", pos.x(), pos.y());
					return false;
				}

				Broodwar->printf("Rightclick hittade ingen path");
			}
		}
	}	
	return false;
}

TilePosition ExampleAIModule::FindFreeBuildSlotInHomeRegion(UnitType buildingType)
{
	//Get Command Center Tile Pos
	TilePosition commandCenterTilePos;
//	Region commandCenterRegion;
	Position commandCenterPosition;
	
	for (std::set<Unit*>::const_iterator i = Broodwar->self()->getUnits().begin(); i != Broodwar->self()->getUnits().end(); i++)
	{
		if ((*i)->getType() == UnitTypes::Terran_Command_Center)
		{
			commandCenterPosition = (*i)->getPosition();
			Broodwar->printf("Command Center Tile Pos: (%d, %d)", (*i)->getTilePosition().x(), (*i)->getTilePosition().y());
		}
	}
	

	int leftBound, rightBound, topBound, bottomBound;
	Position regionCenter = home->getCenter();

	leftBound = Broodwar->getRegionAt(commandCenterPosition)->getBoundsLeft() / TILE_SIZE;
	rightBound = Broodwar->getRegionAt(commandCenterPosition)->getBoundsRight() / TILE_SIZE;
	topBound = Broodwar->getRegionAt(commandCenterPosition)->getBoundsTop() / TILE_SIZE;
	bottomBound = Broodwar->getRegionAt(commandCenterPosition)->getBoundsBottom() / TILE_SIZE;

	


	int width = buildingType.tileWidth();
	int height = buildingType.tileHeight();

	Broodwar->printf("Building Width: %d, Building Height: %d", width, height);

	//plats f�r addons
	if (buildingType == BWAPI::UnitTypes::Terran_Command_Center ||
		buildingType == BWAPI::UnitTypes::Terran_Factory ||
		buildingType == BWAPI::UnitTypes::Terran_Starport ||
		buildingType == BWAPI::UnitTypes::Terran_Science_Facility)
	{
		width += 2;
	}

	for (int i = leftBound; i < rightBound; i++)
	{
		for (int j = topBound; j < bottomBound; j++)
		{
			if (i < Broodwar->mapWidth() && j < Broodwar->mapHeight() && Broodwar->canBuildHere(NULL, TilePosition(i, j), buildingType) && Broodwar->getUnitsOnTile(i, j).size() == 0)
			{
				bool buildable = true;
				//<= f�r att f� en extra tile, s� man ska kunna g� mellan byggnaderna
				for (int k = i; k < (i+width+1); k++)
				{
					for (int l = j; l < (j+height+1); l++)
					{
						//Broodwar->canBuildHere(NULL, TilePosition(k, l), buildingType);
						//!Broodwar->isBuildable(k, l)
						if (k > Broodwar->mapWidth() || l > Broodwar->mapHeight() || !Broodwar->canBuildHere(NULL, TilePosition(k, l), buildingType) && Broodwar->getUnitsOnTile(k, l).size() == 0)
							buildable = false;
					}
				}
				if (buildable)
				{
					Broodwar->printf("Build position found: (%d,%d)", i, j);
					return TilePosition(i, j);
				}
			}
		}
	}
	Broodwar->printf("Couldn't Find Build Position");
	return TilePosition(-1, -1);
	
}

TilePosition ExampleAIModule::FindFreeBuildSlotInCurrentRegion(UnitType buildingType, Position posInRegion)
{
	int leftBound, rightBound, topBound, bottomBound;

	leftBound = Broodwar->getRegionAt(posInRegion)->getBoundsLeft() / TILE_SIZE;
	rightBound = Broodwar->getRegionAt(posInRegion)->getBoundsRight() / TILE_SIZE;
	topBound = Broodwar->getRegionAt(posInRegion)->getBoundsTop() / TILE_SIZE;
	bottomBound = Broodwar->getRegionAt(posInRegion)->getBoundsBottom() / TILE_SIZE;
	
	int width = buildingType.tileWidth();
	int height = buildingType.tileHeight();

	Broodwar->printf("Building Width: %d, Building Height: %d", width, height);

	//plats f�r addons
	if (buildingType == BWAPI::UnitTypes::Terran_Command_Center ||
		buildingType == BWAPI::UnitTypes::Terran_Factory ||
		buildingType == BWAPI::UnitTypes::Terran_Starport ||
		buildingType == BWAPI::UnitTypes::Terran_Science_Facility)
	{
		width += 2;
	}


	for (int i = leftBound; i < rightBound; i++)
	{
		for (int j = topBound; j < bottomBound; j++)
		{
			if (i < Broodwar->mapWidth() && j < Broodwar->mapHeight() && Broodwar->canBuildHere(NULL, TilePosition(i, j), buildingType) && Broodwar->getUnitsOnTile(i, j).size() == 0)
			{
				bool buildable = true;
				for (int k = i; k < (i + width + 1); k++)
				{
					for (int l = j; l < (j + height + 1); l++)
					{
						if (k > Broodwar->mapWidth() || l > Broodwar->mapHeight() || !Broodwar->canBuildHere(NULL, TilePosition(k, l), buildingType) && Broodwar->getUnitsOnTile(k, l).size() == 0)
							buildable = false;
					}
				}
				if (buildable)
				{
					Broodwar->printf("Build position found: (%d,%d)", i, j);
					return TilePosition(i+1, j+1);
				}
			}
		}
	}
	Broodwar->printf("Couldn't Find Build Position");
	return TilePosition(-1, -1);
}

TilePosition ExampleAIModule::FindFreeBuildSlotInHomeArea(UnitType buildingType)
{
	Position regionCenter = home->getCenter();
	TilePosition tilePos;

	tilePos = FindFreeBuildSlotInCurrentRegion(buildingType, regionCenter);
	if (tilePos.x() != -1)
	{
		return tilePos;
	}


	std::set<BWAPI::Region*> regions = Broodwar->getRegionAt(regionCenter)->getNeighbors();
	for (std::set<BWAPI::Region*>::const_iterator c = regions.begin(); c != regions.end(); c++)
	{
		regionCenter = (*c)->getCenter();
		tilePos = FindFreeBuildSlotInCurrentRegion(buildingType, regionCenter);
		if (tilePos.x() != -1)
		{
			Broodwar->printf("Found spot at (%d, %d)", tilePos.x(), tilePos.y());
			return tilePos;
	}
		else
		{
			Broodwar->printf("FindFreeBuildSlotInCurrentRegion returned: (%d, %d)", tilePos.x(), tilePos.y());
		}
	}
	Broodwar->printf("Returnerar -1, -1");
	return TilePosition(-1, -1);
}

TilePosition ExampleAIModule::FindFreeBuildSlotInSimonsArea(UnitType buildingType)
{
	int width = buildingType.tileWidth();
	int height = buildingType.tileHeight();

	//plats f�r addons
	if (buildingType == BWAPI::UnitTypes::Terran_Command_Center ||
		buildingType == BWAPI::UnitTypes::Terran_Factory ||
		buildingType == BWAPI::UnitTypes::Terran_Starport ||
		buildingType == BWAPI::UnitTypes::Terran_Science_Facility)
	{
		width += 2;
	}

	int leftBound, rightBound, upBound, downBound;
	leftBound = Global::left / TILE_SIZE;
	rightBound = Global::right / TILE_SIZE;
	upBound = Global::up / TILE_SIZE;
	downBound = Global::down / TILE_SIZE;
	Broodwar->printf("LeftBound: %d, RightBound: %d, UpBound: %d, BottomBound: %d", leftBound, rightBound, upBound, downBound);
	for (int i = leftBound; i < rightBound; i++)
	{
		for (int j = upBound; j < downBound; j++)
		{
			if (Broodwar->canBuildHere(NULL, TilePosition(i, j), buildingType) && Broodwar->getUnitsOnTile(i, j).size() == 0 && Global::startTCCPos.hasPath(Position(i * 32, j * 32)))
			{
				bool buildable = true;
				for (int k = i; k < (i + width + 1); k++)
				{
					for (int l = j; l < (j + height + 1); l++)
					{
						if (!Broodwar->canBuildHere(NULL, TilePosition(k, l), buildingType) || !(Broodwar->getUnitsOnTile(k, l).size() == 0))
							buildable = false;
					}
				}
				if (buildable)
				{
					Broodwar->printf("Build position found: (%d,%d)", i, j);
					return TilePosition(i + 1, j + 1);
				}
			}
		}
	}

	return TilePosition(-1, -1);
}

bool ExampleAIModule::CanBuildHere(TilePosition position, UnitType buildingType)
{
	int width = buildingType.tileWidth();
	int height = buildingType.tileHeight();

	//plats f�r addons
	if (buildingType == BWAPI::UnitTypes::Terran_Command_Center ||
		buildingType == BWAPI::UnitTypes::Terran_Factory ||
		buildingType == BWAPI::UnitTypes::Terran_Starport ||
		buildingType == BWAPI::UnitTypes::Terran_Science_Facility)
	{
		width += 2;
	}
	int x = position.x();
	int y = position.y();
	for (int i = x; i < x + width; i++)
	{
		for (int j = y; j < y + height; j++)
		{
			if (i > Broodwar->mapWidth() || j > Broodwar->mapHeight() || j < 0 || i < 0 || !Broodwar->canBuildHere(NULL, TilePosition(i, j), buildingType, true) || !(Broodwar->getUnitsOnTile(i, j).size() == 0))
				return false;
		}
	}
	return true;
}
TilePosition ExampleAIModule::FindFreeBuildSlotSpiral(UnitType buildingType, Position spiralStartPos)
{
	/*
	=====|=====|====|====|====|====|====|
	     |	   |    |    |    |    |    |
	=====|=====|====|====|====|====|====|
	     |	   |  6 |  5 | 4  |    |    |
	=====|=====|====|====|====|====|====|
	     |	   |  7 |  C | 3  |    |    |
	=====|=====|====|====|====|====|====|
	     |	   |  8 |  1 | 2  |    |    |
	=====|=====|====|====|====|====|====|
	     |	   |  9 | 10 | 11 | 12 |    |
	=====|=====|====|====|====|====|====|
	     |	   |    |    |    |    |    |
	
	*/
	int length = 1;
	int x = spiralStartPos.x() / TILE_SIZE;
	int y = spiralStartPos.y() / TILE_SIZE;
	int dx = 0;
	int dy = 1;
	int currentLength = 0;
	bool changeLength = false;

	while (length < Broodwar->mapWidth())
	{
		if (CanBuildHere(TilePosition(x, y), buildingType))
			return TilePosition(x, y);

		x += dx;
		y += dy;
		currentLength++;

		if (currentLength == length)
		{
			currentLength = 0;

			if (changeLength)
				length++;

			changeLength = !changeLength;

			//Kontrollera v�ndningar i spiralen
			if (dx == 0)
			{
				dx = dy;
				dy = 0;
			}
			else
			{
				dy = -dx;
				dx = 0;
		}
	}


	}
	Broodwar->printf("Didn't find build spot");
	return TilePosition(-1, -1);



}

bool ExampleAIModule::IsTileReserved(TilePosition tilePos)
{
	for (unsigned int c = 0; c < reservedTiles.size(); c++)
	{
		if (reservedTiles[c].x() == tilePos.x() && reservedTiles[c].y() == tilePos.y())
			return true;
	}
	return false;
}