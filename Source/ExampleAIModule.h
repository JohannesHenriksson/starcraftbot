#pragma once
#include <BWAPI.h>

#include <BWTA.h>
#include <windows.h>
#include "../Agent.h"
#include "../Barrack.h"
#include "../Building.h"
#include "../Marine.h"
#include "../UnitAgent.h"
#include "../TCC.h"
#include "../Worker.h"
#include "../Global.h"
#include "../Medic.h"
#include "../Academy.h"
#include "../Refinery.h"
#include "../Factory.h"
#include "../SiegeTank.h"
#include "../MachineShopAddon.h"
#include "../Worker.h"

extern bool analyzed;
extern bool analysis_just_finished;
extern BWTA::Region* home;
extern BWTA::Region* enemy_base;
DWORD WINAPI AnalyzeThread();

using namespace BWAPI;
using namespace BWTA;

class ExampleAIModule : public BWAPI::AIModule
{
public:
	//Methods inherited from BWAPI:AIModule
	virtual void onStart();
	virtual void onEnd(bool isWinner);
	virtual void onFrame();
	virtual void onSendText(std::string text);
	virtual void onReceiveText(BWAPI::Player* player, std::string text);
	virtual void onPlayerLeft(BWAPI::Player* player);
	virtual void onNukeDetect(BWAPI::Position target);
	virtual void onUnitDiscover(BWAPI::Unit* unit);
	virtual void onUnitEvade(BWAPI::Unit* unit);
	virtual void onUnitShow(BWAPI::Unit* unit);
	virtual void onUnitHide(BWAPI::Unit* unit);
	virtual void onUnitCreate(BWAPI::Unit* unit);
	virtual void onUnitDestroy(BWAPI::Unit* unit);
	virtual void onUnitMorph(BWAPI::Unit* unit);
	virtual void onUnitRenegade(BWAPI::Unit* unit);
	virtual void onSaveGame(std::string gameName);
	virtual void onUnitComplete(BWAPI::Unit *unit);

	//Own methods
	void drawStats();
	void drawTerrainData();
	void showPlayers();
	void showForces();
	Position findGuardPoint();

	static void onRefineryCreate();

	//V�ra egna
	int phase;
	bool depotInQueue;
	bool academyHasBeenBuilt;
	void buildSupplyDepot();
	bool canBuildHereWithSpace(BWAPI::TilePosition position, BWAPI::UnitType type, int buildDist) const;
	TilePosition FindFreeBuildSlotInHomeRegion(UnitType buildingType);
	TilePosition FindFreeBuildSlotInCurrentRegion(UnitType buildingType, Position posInRegion);
	bool Build(UnitType buildingType, TilePosition buildingPosition);
	bool Build(UnitType buildingType);
	TilePosition FindFreeBuildSlotInHomeArea(UnitType buildingType);
	TilePosition FindFreeBuildSlotInSimonsArea(UnitType buildingType);
	TilePosition FindFreeBuildSlotSpiral(UnitType buildingType, Position spiralStartPos);
	bool CanBuildHere(TilePosition position, UnitType buildingType);
	bool IsTileReserved(TilePosition tilePos);

	std::vector<TilePosition> reservedTiles;

};
