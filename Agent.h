#pragma once
#include <BWAPI.h>
#include <BWTA.h>
#include <algorithm>

using namespace BWAPI;
using namespace BWTA;

class Agent
{
public:
	enum AgentType
	{
		Type_Worker,
		Type_Building,
		Type_CommandCenter,
		Type_Barrack,
		Type_Unit,
		Type_MilitaryUnit,
		Type_Marine,
		Type_Medic,
		Type_Refinery,
		Type_Academy,
		Type_Factory,
		Type_SiegeTank,
		Type_MachineShopAddon,
		Type_Undefined
	};

	Agent(){};
	Agent(Unit* unit);
	~Agent();
	virtual bool Update();
	AgentType getAgentType(){ return agentType; };
	Unit* getUnit(){ return unit; };
protected:
	Unit* unit;
	AgentType agentType;

};