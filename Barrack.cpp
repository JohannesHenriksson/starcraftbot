#include "Barrack.h"


Barrack::Barrack()
{
}

Barrack::Barrack(Unit* unit)
:Building(unit)
{
	agentType = Type_Barrack;
	unit->setRallyPoint(Global::baseChokePoint);
}


Barrack::~Barrack()
{
}

bool Barrack::Update()
{

	if (!unit->isBeingConstructed())
	{
		if (Global::nrOfMarines < Global::plannedMarines && !unit->isTraining() && ((Broodwar->self()->minerals() - Global::plannedSpentMinerals) >= UnitTypes::Terran_Marine.mineralPrice()))
		{
			unit->train(UnitTypes::Terran_Marine);
		}
		else if (Global::nrOfMedics < 3 && !unit->isTraining() && (Broodwar->self()->minerals() - Global::plannedSpentMinerals) >= UnitTypes::Terran_Marine.mineralPrice() && (Broodwar->self()->gas() - Global::plannedSpentGas) >= UnitTypes::Terran_Medic.gasPrice())
		{
			unit->train(UnitTypes::Terran_Medic);
		}
	}
	return Building::Update();
}