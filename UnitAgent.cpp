#include "UnitAgent.h"
#include "Global.h"

UnitAgent::UnitAgent(Unit* unit)
	:Agent(unit)
{
	attack = false;
	agentType = Type_Unit;
}


UnitAgent::~UnitAgent()
{
}

bool UnitAgent::Update()
{
	//Broodwar->printf("UnitAgent: %s", unit->getType().getName());
	if (attack && unit->isIdle())
		unit->attack(Global::enemyBase);
	if (Global::nrOfMarines >= 10 && Global::nrOfMedics >= 3 && Global::nrOfSiegeTanks >= 3)
	{
		attack = true;
	}
	return Agent::Update();
}