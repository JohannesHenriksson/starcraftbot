#pragma once
#include "Agent.h"

class UnitAgent : 
	public Agent
{
public:
	UnitAgent(){};
	UnitAgent(Unit* unit);
	~UnitAgent();
	virtual bool Update();

	bool attack;
};

