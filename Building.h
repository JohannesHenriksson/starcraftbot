#pragma once
#include "Agent.h"

class Building :
	public Agent
{
public:
	Building(){};
	Building(Unit* unit);
	~Building();
	virtual bool Update();
};

