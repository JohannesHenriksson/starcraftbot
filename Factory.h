#pragma once
#include "Building.h"
#include "Global.h"
class Factory :
	public Building
{
public:
	Factory(){};
	Factory(Unit* unit);
	~Factory();
	bool Update();
	bool hasAddon;
	bool gotResources(UnitType unitType);

};

