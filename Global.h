#pragma once
#include "Source\ExampleAIModule.h"

class Global
{
public:
	static int right;
	static int left;
	static int up;
	static int down;
	static Position startTCCPos;
	static Position baseChokePoint;
	static Position enemyBase;
	static std::vector<UnitType> nextBuilding;

	//Gas and Minerals
	static int plannedSpentMinerals;
	static int plannedSpentGas;

	//Number of Units
	static int nrOfWorkers;
	static int nrOfGasMiners;
	static int nrOfMarines;
	static int nrOfMedics;
	static int nrOfSiegeTanks;

	//Number of buildings
	static int nrOfCommandCenters;
	static int nrOfBarracks;
	static int nrOfSupplyDepots;
	static int nrOfRefineries;
	static int nrOfFactories;
	static int nrOfAcademies;

	//Planned Numbers
	static int plannedSiegeTanks;
	static int plannedMarines;
	static int plannedGasMiners;
protected:
private:
};
