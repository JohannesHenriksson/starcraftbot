#include "SiegeTank.h"



SiegeTank::SiegeTank(Unit* unit)
:UnitAgent(unit)
{
	agentType = Type_SiegeTank;
}


SiegeTank::~SiegeTank()
{
}

bool SiegeTank::Update()
{
	std::set<Unit*> unitsInRange = unit->getUnitsInRadius(384);
	if (unit->getOrder() == Orders::Sieging || unit->getOrder() == Orders::Unsieging)
	{
		return UnitAgent::Update();
	}
	if (unit->isSieged())
	{
		for (std::set<Unit*>::const_iterator i = unitsInRange.begin(); i != unitsInRange.end(); i++)
		{
			if ((*i)->getPlayer() == Broodwar->enemy())
			{
				if (unit->getDistance((*i)->getPosition()) > 64 && (*i)->isVisible())
				{
					unit->attack(*i);
					return UnitAgent::Update();
				}
				else
					Broodwar->printf("distance: %d, isVisible: %s", unit->getDistance((*i)->getPosition()), (*i)->isVisible());
			}
		}
		unit->unsiege();
	}
	else
	{
		for (std::set<Unit*>::const_iterator i = unitsInRange.begin(); i != unitsInRange.end(); i++)
		{
			if ((*i)->getPlayer() == Broodwar->enemy())
			{

				if (unit->getDistance((*i)->getPosition()) > 64 && (*i)->isVisible())
				{
					unit->siege();
					return UnitAgent::Update();
				}
				else
					Broodwar->printf("distance: %d, isVisible: %s", unit->getDistance((*i)->getPosition()), (*i)->isVisible());
			}
		}
	}
	return UnitAgent::Update();
}
