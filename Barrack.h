#pragma once
#include "Building.h"
#include "Source\ExampleAIModule.h"
#include "Global.h"

class Barrack :
	public Building
{
public:
	Barrack();
	Barrack(Unit* unit);
	~Barrack();
	bool Update();
};

