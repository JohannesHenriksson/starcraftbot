#pragma once
#include "Building.h"
#include "Global.h"
class MachineShopAddon
	:public Building
{
public:
	MachineShopAddon(){};
	MachineShopAddon(Unit* unit);
	~MachineShopAddon();

	bool Update();
};

