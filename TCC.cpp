#include "TCC.h"


TCC::TCC(Unit* unit)
	:Building(unit)
{
	agentType = Type_CommandCenter;
}


TCC::~TCC()
{
}

bool TCC::Update()
{
	if (Broodwar->self()->minerals() - Global::plannedSpentMinerals >= UnitTypes::Terran_SCV.mineralPrice() && !unit->isTraining() && Global::nrOfWorkers < 22)
	{
		unit->train(UnitTypes::Terran_SCV);
	}
	return Building::Update();
}